package wahidah.hildha.uploadfoto

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.provider.MediaStore
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imUpload -> {
                requestPermissions()
            }
            R.id.btnInsert -> {
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUpdate -> {
                queryInsertUpdateDelete("update")
            }
            R.id.btnDelete -> {
                queryInsertUpdateDelete("delete")
            }
            R.id.btnFind -> {
                showDataMhs(edNamaMhs.text.toString().trim())
            }
        }
    }

    lateinit var mediaHelper: MediaHelper
    lateinit var mediaHelperCamera: MediaHelperCamera
    lateinit var mhsAdapter : AdapterDataMhs
    lateinit var prodiAdapter : ArrayAdapter<String>
    var daftarMhs = mutableListOf<HashMap<String,String>>()
    var daftarProdi = mutableListOf<String>()
    val url = "http://192.168.43.81/kampuss/show_data.php"
    val url2 = "http://192.168.43.81/kampuss/get_nama_prodi.php"
    val url3 = "http://192.168.43.81/kampuss/query_IUD.php"
    var pilihProdi = ""
    var imstr = ""
    var namafile = ""
    var fileUri = Uri.parse("")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        try {
            val m = StrictMode:: class.java.getMethod("disableDeathOnFileUriExposure")
            m.invoke(null)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        mhsAdapter = AdapterDataMhs(daftarMhs, this)
        prodiAdapter = ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, daftarProdi)
        spinProdi.adapter = prodiAdapter
        mediaHelper = MediaHelper(this)
        mediaHelperCamera = MediaHelperCamera()
        listMhs.layoutManager = LinearLayoutManager(this)
        listMhs.adapter = mhsAdapter

        spinProdi.onItemSelectedListener = itemSelected
        imUpload.setOnClickListener(this)
        //listMhs.addOnItemTouchListener(itemTouch)
        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDataMhs("")
        getNamaProdi()
    }

//    val itemTouch = object : RecyclerView.OnItemTouchListener{
//        override fun onTouchEvent(p0: RecyclerView, p1: MotionEvent) {}
//
//        override fun onInterceptTouchEvent(p0: RecyclerView, p1: MotionEvent): Boolean {
//            val view = p0.findChildViewUnder(p1.x,p1.y)
//            val tag = p0.getChildAdapterPosition(view!!)
//            val pos = daftarProdi.indexOf(daftarMhs.get(tag).get("nama_prodi"))
//
//            edNim.setText(daftarMhs.get(tag).get("nim").toString())
//            edNamaMhs.setText(daftarMhs.get(tag).get("nama").toString())
//            Picasso.get().load(daftarMhs.get(tag).get("url")).into(imUpload)
//            spinProdi.setSelection(pos)
//            return false
//        }
//
//        override fun onRequestDisallowInterceptTouchEvent(p0: Boolean) {}
//    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinProdi.setSelection(0)
            pilihProdi = daftarProdi.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihProdi = daftarProdi.get(position)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK)
            if (requestCode == mediaHelperCamera.getRcCamera()) {
                imstr = mediaHelperCamera.getBitmapToString(imUpload, fileUri)
                namafile =mediaHelperCamera.getMyFileName()
            }
    }


    fun getNamaProdi(){
        val request = StringRequest(
            Request.Method.POST, url2,
            Response.Listener {
                    response ->
                daftarProdi.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarProdi.add(jsonObject.getString("nama_prodi"))
                }
                prodiAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener {
                    error ->
            }
        )
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


    fun showDataMhs(namaMhs: String){
        val request = object : StringRequest(
            Request.Method.POST, url, Response.Listener{ response ->
                daftarMhs.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mhs = HashMap<String,String>()
                    mhs.put("nim",jsonObject.getString("nim"))
                    mhs.put("nama",jsonObject.getString("nama"))
                    mhs.put("nama_prodi",jsonObject.getString("nama_prodi"))
                    mhs.put("url",jsonObject.getString("url"))
                    daftarMhs.add(mhs)
                }
                mhsAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener{ error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                hm.put("nama", namaMhs)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST, url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if (error.equals("000")){
                    //Toast.makeText(this, "Berhasil", Toast.LENGTH_LONG).show()
                    showDataMhs("")

                } else {
                    Toast.makeText(this, "Gagal melakukan Operasi", Toast.LENGTH_LONG).show()
                }
                mhsAdapter.notifyDataSetChanged()
                when(mode){
                    "insert" ->{
                        Toast.makeText(this, "Berhasil insert data", Toast.LENGTH_LONG).show()
                    }
                    "update" ->{
                        Toast.makeText(this, "Berhasil update data", Toast.LENGTH_LONG).show()
                    }
                    "delete" ->{
                        Toast.makeText(this, "Berhasil delete data", Toast.LENGTH_LONG).show()
                    }
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()

            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                when(mode){
                    "insert" ->{
                        hm.put("mode", "insert")
                        hm.put("nim", edNim.text.toString())
                        hm.put("nama", edNamaMhs.text.toString())
                        hm.put("image", imstr)
                        hm.put("file", namafile)
                        hm.put("nama_prodi", pilihProdi)
                    }
                    "update" ->{
                        hm.put("mode", "update")
                        hm.put("nim", edNim.text.toString())
                        hm.put("nama", edNamaMhs.text.toString())
                        hm.put("image", imstr)
                        hm.put("file", namafile)
                        hm.put("nama_prodi", pilihProdi)
                    }
                    "delete" ->{
                        hm.put("mode", "delete")
                        hm.put("nim", edNim.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    fun requestPermissions() = runWithPermissions(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA){
        fileUri = mediaHelperCamera.getOutputMediaFileUri()
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
        startActivityForResult(intent, mediaHelperCamera.getRcCamera())

    }
//    fun uploadFile(){
//        val request = object : StringRequest(Method.POST,url4,
//            Response.Listener { response ->
//                val jsonObject = JSONObject(response);
//                val kode = jsonObject.getString("kode")
//                if (kode.equals("000")) {
//                    Toast.makeText(this, "Upload Foto Sukses", Toast.LENGTH_SHORT).show()
//                }else {
//                    Toast.makeText(this, "Upload Foto Gagal", Toast.LENGTH_SHORT).show()
//
//                }
//            },
//            Response.ErrorListener { error ->
//                Toast.makeText(this, "Tidak dapat tersambung ke server", Toast.LENGTH_SHORT).show()
//
//            }) {
//            override fun getParams(): MutableMap<String, String> {
//                val hm = HashMap<String, String>()
//                hm.put("imstr", imstr)
//                hm.put("namafile", namafile)
//                return hm
//            }
//        }
//        val q = Volley.newRequestQueue(this)
//        q.add(request)
//    }
}
